#!/usr/bin/env python3
from flask import Flask,render_template
from data import posts

app = Flask(__name__)

Posts = posts()

@app.route("/")
def index():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/posts")
def posts():
    return render_template("posts.html",posts = Posts)

@app.route("/post/<string:idx>/")
def post(idx):
    return render_template("post.html",idx=idx)



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port='8080')