##PYBOOK 

A site written in Python for Python. This will probably be a tutorial/blog/forum/news site about latest updates on the
Python Django/Flask community as well as being a tutorial site. 

What's so unique about this site you ask?
Well nothing you say.

Then let me ask you a question. What's so unique about you?
Well nothing again. Aren't we all in the same template.
What makes us unique is the user base of us, (our friends, family , the people around us who interact (use) with us). 
You can't be replaced by me because of the users who had already interacted with you. The familiarity with you will make them reject me.
 
In the same way, we believe this site will be unique because of the users who interact.

Dev environment:
- Lubuntu 18.04 bionic beaver

**VM Setup:** (If you have trouble port forwarding GUEST to HOST)
-------------
**Paravirtualized Network* (virtio-net) is the key**

1) Select your vm that you want for port forwarding.
NOTE: Make sure the vm is in stop mode.

2) Select “Settings"

3) Select “Network"

4) Select any free Adapter. Lets say "Adapter 2"

5) Enable "Enable Network Adapter"

6) In Attached to dropdown, select "NAT"

7) Select "Advanced"

8) In Adapter Type: Select “Paravirtualized Network (virtio-net)” [ This is important ]

9) Select “Port Forwarding"

10) In Right hand part of the “port forwarding” dialog box, select “+"

11) Provide the Name: Anything you want. example for ssh, say “blah"

Protocol: Type of protocol [ TCP ] 

Host IP: provide hostname from which host you want to connect to [ Leave blank]

Host Port: On what port of that Host you want to connect to remote port [ 8080 ]

Guest IP: Leave it Blank

Guest Port: To what port you want to connect from the above host. [ 8080 ]

-------------


**Usage**:

Install MySQL:

`sudo apt-get install mysql-server libmysqlclient-dev`

Update git to latest version:

`sudo add-apt-repository ppa:git-core/ppa -y`

`sudo apt-get update`

`sudo apt-get install git -y`

`git --version`



--
**More to be added - requirements.txt,  use-case etc**

**Clone a repository**
Clone as mentioned above - git clone 'url'

Hoping to see more contributions from people all over the world.

Thanks!!
